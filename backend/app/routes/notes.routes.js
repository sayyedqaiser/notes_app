module.exports = (app) => {
  const notes = require("../controllers/notes.controller.js");
  const { authJwt } = require("../middleware");

  var router = require("express").Router();

  router.post("/",[authJwt.verifyToken], notes.create);

  router.get("/",[authJwt.verifyToken], notes.findAll);

  router.get("/:id",[authJwt.verifyToken], notes.findOne);

  router.put("/:id",[authJwt.verifyToken], notes.update);

  router.delete("/:id",[authJwt.verifyToken], notes.delete);

  router.delete("/", notes.deleteAll);

  app.use("/api/notes", router);
};
