module.exports = {
  HOST: "localhost",
  USER: "postgres",
  PASSWORD: "postgres",
  DB: "notes",
  dialect: "postgres",

  // pool: {
  //   max: 5,
  //   min: 0,
  //   acquire: 30000,
  //   idle: 10000
  // }
};

// module.exports = {
//   HOST: "ep-icy-meadow-03427326-pooler.us-east-1.postgres.vercel-storage.com",
//   USER: "default",
//   PASSWORD: "hI6Wv2nEBRsg",
//   DB: "verceldb",
//   dialect: "postgres",
//   dialectOptions: {
//       ssl: {
//           require: true, // Enable SSL
//           requestCert: true,
//       }
//   }
//   // pool: {
//   //   max: 5,
//   //   min: 0,
//   //   acquire: 30000,
//   //   idle: 10000
//   // }
// };
