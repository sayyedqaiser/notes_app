import { Component, EventEmitter, Output } from '@angular/core';
import { Notes } from 'src/app/models/notes.model';
import { NotesService } from 'src/app/services/notes.service';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-add-note',
  templateUrl: './add-note.component.html',
  styleUrls: ['./add-note.component.css']
})
export class AddNoteComponent {

  @Output("refreshList") refreshList: EventEmitter<any> = new EventEmitter();

  notes: Notes = {
    title: '',
    description: '',
  };
  submitted = false;

  user = this.storageService.getUser();

  constructor(private notesService: NotesService ,private storageService: StorageService) { }

  ngOnInit(): void {
  }

  saveNote(): void {
    const data = {
      title: this.notes.title,
      description: this.notes.description,
      user_id : this.user.id
    };

    this.notesService.create(data)
      .subscribe({
        next: (res) => {
          console.log(res);
          this.submitted = true;
          this.refreshList.emit();
        },
        error: (e) => console.error(e)
      });
  }

  newNote(): void {
    this.submitted = false;
    this.notes = {
      title: '',
      description: '',
    };
  }
}
