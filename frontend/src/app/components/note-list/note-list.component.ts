import { Component } from '@angular/core';
import { Notes } from 'src/app/models/notes.model';
import { NotesService } from 'src/app/services/notes.service';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-note-list',
  templateUrl: './note-list.component.html',
  styleUrls: ['./note-list.component.css']
})
export class NoteListComponent {

  notes?: Notes[];
  currentNotes: Notes = {};
  currentIndex = -1;
  title = '';
  user = this.storageService.getUser();

  constructor(private notesService: NotesService ,private storageService: StorageService) { }

  ngOnInit(): void {
    this.retrieveNotes();
  }

  retrieveNotes(): void {
    this.notesService.getAll()
      .subscribe({
        next: (data) => {
          this.notes = data;
          console.log(data);
        },
        error: (e) => console.error(e)
      });
  }

  refreshList(): void {    
    this.retrieveNotes();
    this.currentNotes = {};
    this.currentIndex = -1;
  }

  setActiveNote(notes: Notes, index: number): void {
    this.currentNotes = notes;
    this.currentIndex = index;
  }

  searchTitle(): void {
    this.currentNotes = {};
    this.currentIndex = -1;

    this.notesService.findByTitle(this.title)
      .subscribe({
        next: (data) => {
          this.notes = data;
          console.log(data);
        },
        error: (e) => console.error(e)
      });
  }

}
