import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Notes } from 'src/app/models/notes.model';
import { NotesService } from 'src/app/services/notes.service';

@Component({
  selector: 'app-note-details',
  templateUrl: './note-details.component.html',
  styleUrls: ['./note-details.component.css']
})
export class NoteDetailsComponent {
  @Input() viewMode = false;

  @Input() currentNotes: Notes = {
    title: '',
    description: '',
  };

  @Output("refreshList") refreshList: EventEmitter<any> = new EventEmitter();

  message = '';

  constructor(
    private notesService: NotesService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    if (!this.viewMode) {
      this.message = '';
      this.getNote(this.route.snapshot.params["id"]);
    }
  }

  

  getNote(id: string): void {
    this.notesService.get(id)
      .subscribe({
        next: (data) => {
          this.currentNotes = data;
          console.log(data);
        },
        error: (e) => console.error(e)
      });
  }

  changemode() : void{
    console.log('mode chage');
    this.viewMode = false;
  }

  updateNote(): void {
    this.message = '';

    this.notesService.update(this.currentNotes.id, this.currentNotes)
      .subscribe({
        next: (res) => {
          console.log(res);
          this.message = res.message ? res.message : 'This tutorial was updated successfully!';
        this.viewMode = true;

        },
        error: (e) => console.error(e)
      });
  }

  deleteNote(): void {
    this.notesService.delete(this.currentNotes.id)
      .subscribe({
        next: (res) => {
          console.log(res);
          this.refreshList.emit();
        },
        error: (e) => console.error(e)
      });
    
    }
}
