import { Injectable } from '@angular/core';
import { Notes } from '../models/notes.model';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { StorageService } from './storage.service';
const baseUrl = 'http://localhost:8080/api/notes';

@Injectable({
  providedIn: 'root'
})



export class NotesService {

  constructor(private http: HttpClient,private storageService: StorageService ) { }
  user = this.storageService.getUser();

  headers = new HttpHeaders({
    'x-access-token': this.user.accessToken, 
  });
  

  getAll(): Observable<Notes[]> {
  
    return this.http.get<Notes[]>(baseUrl,{ headers: this.headers });
  }

  get(id: any): Observable<Notes> {
    return this.http.get(`${baseUrl}/${id}`,{ headers: this.headers });
  }

  create(data: any): Observable<any> {
    return this.http.post(baseUrl, data,{ headers: this.headers });
  }

  update(id: any, data: any): Observable<any> {
    return this.http.put(`${baseUrl}/${id}`, data,{ headers: this.headers });
  }

  delete(id: any): Observable<any> {
    return this.http.delete(`${baseUrl}/${id}`,{ headers: this.headers });
  }

  deleteAll(): Observable<any> {
    return this.http.delete(baseUrl);
  }

  findByTitle(title: any): Observable<Notes[]> {
    return this.http.get<Notes[]>(`${baseUrl}?title=${title}`,{ headers: this.headers });
  }
}
