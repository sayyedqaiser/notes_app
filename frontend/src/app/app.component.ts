import { Component } from '@angular/core';
import { StorageService } from './services/storage.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'My Notes App';

  user = this.storageService.getUser();
  constructor(private storageService: StorageService) { }


  logout(): void {
    this.storageService.clean();
    window.location.reload();
  }
}
